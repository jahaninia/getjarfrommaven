package ir.jol.main;

import org.apache.commons.io.FileUtils;

import java.io.*;
import java.time.ZonedDateTime;
import java.util.*;


public class Main {
    public static void main(String[] args) {
        //Get Data User
        String user = System.getProperty("user.name");
        String homeUser = System.getProperty("user.home") + File.separator + ".m2" + File.separator + "repository" + File.separator;
        String os = System.getProperty("os.name");
        String dirApp = System.getProperty("user.dir") + File.separator + "target" + File.separator;

        //Show Data
        System.out.println("User: " + user + "\n");
        System.out.println("OS: " + os + "\n");
        System.out.println("Home User: " + homeUser);
        System.out.println("TargetPath: " + dirApp);
        //Check make list
        System.out.println("Make defualt Package Of Mavan press 2");
        String createListDefault = new Scanner(System.in).nextLine();

        try {


            //check exists target folder
            File target = new File(dirApp);
            if (target.exists() == false) {
                if (target.mkdir() == false) {
                    System.out.println("Can't create folder targe in " + System.getProperty("user.dir") + "Please create manual folder target !!!");
                    System.exit(-1);
                }

            }


            List<File> listFile = new ArrayList<>();
            List<String> currentFilesInMaven = new ArrayList<>();
            List<String> listFileDefaultMavin = new ArrayList<>();
            listfilesOfDirectory(homeUser, listFile);

            listFile.forEach(file -> {
                if (file.getName().endsWith(".jar")) currentFilesInMaven.add(file.getPath());
            });

            File listOfJARFiles = new File(dirApp + File.separator + "listOfJARFiles.txt");
            if (listOfJARFiles.exists() == false) createDefualtListMaven(currentFilesInMaven, dirApp);

            if (createListDefault.equals("2")) {
                createDefualtListMaven(currentFilesInMaven, dirApp);
                System.out.println("File Default Maven is create!!! in Path: " + dirApp);
            }

            File defaultListMaven = new File(dirApp + File.separator + "listOfJARFiles.txt");
            FileReader fileReader = new FileReader(defaultListMaven);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String line;

            while ((line = bufferedReader.readLine()) != null) {
                listFileDefaultMavin.add(line);
            }
            fileReader.close();


            currentFilesInMaven.removeAll(listFileDefaultMavin);

            for (String str : currentFilesInMaven) {
                System.out.println("    >>>>" + str);
                File source = new File(str);
                FileUtils.copyFile(source, new File(dirApp + File.separator + source.getName()));

            }


            System.out.println("*********END***************");

        } catch (Exception e) {
            System.out.println("Error>>>>");
            e.printStackTrace();
        }

    }

    public static void listfilesOfDirectory(String directoryName, List<File> files) {
        File directory = new File(directoryName);

        // Get all files from a directory.
        File[] fList = directory.listFiles();
        if (fList != null)
            for (File file : fList) {
                if (file.isFile()) {
                    files.add(file);
                } else if (file.isDirectory()) {
                    listfilesOfDirectory(file.getAbsolutePath(), files);
                }
            }
    }


    public static void createDefualtListMaven(List<String> setFilePath, String pathMavenList) throws IOException {
        File listJarFiles = new File(pathMavenList + File.separator + "listOfJARFiles.txt");
        listJarFiles.renameTo(new File(pathMavenList + File.separator + "listOfJARFiles.txt-" + ZonedDateTime.now().toInstant().toEpochMilli()));
        FileOutputStream fileOutputStream = new FileOutputStream(listJarFiles, false);
        BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(fileOutputStream));
        for (String str : setFilePath) {
            bufferedWriter.write(str);
            bufferedWriter.newLine();
        }
        bufferedWriter.close();

    }


}
